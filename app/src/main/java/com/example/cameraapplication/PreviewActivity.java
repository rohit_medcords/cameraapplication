package com.example.cameraapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class PreviewActivity extends AppCompatActivity {
    private ImageView ivCapturedImage, ivRetake, ivDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ivCapturedImage = (ImageView) findViewById(R.id.ivCapturedImage);
        ivRetake = (ImageView) findViewById(R.id.ivRetake);
        ivDone = (ImageView) findViewById(R.id.ivDone);
        Bundle bundle = getIntent().getExtras();
        final String filePath = (String) bundle.get("FilePath");
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);
        ivCapturedImage.setImageBitmap(bitmap);
        ivRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("action", "retake");
                setResult(Camera2Activity.CAMERA_2_PREVIEW_ACTIVITY, intent);
                finish();
            }
        });
        ivDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("action", "done");
                setResult(Camera2Activity.CAMERA_2_PREVIEW_ACTIVITY, intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("action", "retake");
        setResult(Camera2Activity.CAMERA_2_PREVIEW_ACTIVITY, intent);
        super.onBackPressed();
    }
}
