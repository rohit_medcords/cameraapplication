package com.example.cameraapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<String> imagePaths;
    private Context context;
    private UploadRecordsActivity activity;
    private int screenWidth, screenHeight;
    private BitmapFactory.Options options;

    public ImageAdapter(Context c, ArrayList<String> imagePaths) {
        context = c;
        this.imagePaths = imagePaths;
        this.activity = (UploadRecordsActivity) context;
        screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        screenHeight = context.getResources().getDisplayMetrics().heightPixels;
        options = new BitmapFactory.Options();
    }

    private class ViewHolderPreviewImage extends RecyclerView.ViewHolder {
        private TextView numberTextView;
        private ImageView imageView;

        private ViewHolderPreviewImage(View view) {
            super(view);

            numberTextView = (TextView) view.findViewById(R.id.photo_number_indicator);
            imageView = (ImageView) view.findViewById(R.id.image);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
            View view2 = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.image_preview_item, viewGroup, false);
            viewHolder = new ViewHolderPreviewImage(view2);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int imagePos) {
        final int position = imagePos;
            final ViewHolderPreviewImage vh1 = (ViewHolderPreviewImage) viewHolder;
            vh1.numberTextView.setText(String.valueOf(imagePos + 1));
            Bitmap bitmap = BitmapFactory.decodeFile(imagePaths.get(imagePos), options);
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, screenWidth, screenHeight, true);
            vh1.imageView.setImageBitmap(scaled);
            vh1.imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });
    }

    @Override
    public int getItemCount() {
        return imagePaths.size();
    }// add +1 for add more option

}
