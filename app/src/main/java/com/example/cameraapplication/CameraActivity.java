package com.example.cameraapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.example.cameraapplication.Utility.getCameraInstance;

public class CameraActivity extends AppCompatActivity {

    @SuppressWarnings("deprecation")
    private Camera mCamera;
    private CameraPreview mPreview;
    ImageButton captureButton, flashButton, singleModeButton, batchModeButton, doneButton;
    private LinearLayout llDone;
    private View singleModeDot, batchModeDot;
    private ImageView imagePreview;
    private TextView noOfImagesTextView;
    private LinearLayout singleModeLayout, batchModeLayout;
    private FrameLayout preview;
    private Boolean flashOn = false;
    private Boolean singleModeOn = false;
    private int noOfImagesTaken = 0;
    private ArrayList<String> imagesAddressList = new ArrayList<>();
    private Boolean afterActivityResult = false;
    public static int CAMERA_PREVIEW_ACTIVITY = 2;
    private String callForm = "";

    @SuppressWarnings("deprecation")
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            noOfImagesTaken++;
            if (!singleModeOn && noOfImagesTaken == 1) {
                batchModeLayout.setVisibility(View.GONE);
                singleModeLayout.setVisibility(View.GONE);
                llDone.setVisibility(View.VISIBLE);
            }
            if (noOfImagesTaken > 1) {
                noOfImagesTextView.setVisibility(View.VISIBLE);
                noOfImagesTextView.setText(String.valueOf(noOfImagesTaken));
            }
            if (data != null) {
                try {
                    File mPictureFile = Utility.createStorageFile(getApplicationContext());
                    OutputStream output = null;
                    if (mPictureFile != null) {
                        output = new FileOutputStream(mPictureFile);
                        output.write(data);
                        imagesAddressList.add(mPictureFile.getAbsolutePath());
                        mPreview.safeToTakePicture = true;
                        Intent intent = new Intent(CameraActivity.this, ImagePreviewActivity.class);
                        intent.putExtra("FilePath", mPictureFile.getAbsolutePath());
                        startActivityForResult(intent, CAMERA_PREVIEW_ACTIVITY);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        if (getIntent().getStringExtra("call") != null)
            callForm = getIntent().getStringExtra("call");
        preview = (FrameLayout) findViewById(R.id.camera_preview);
        captureButton = (ImageButton) findViewById(R.id.button_capture);
        flashButton = (ImageButton) findViewById(R.id.flash_button);
        singleModeButton = (ImageButton) findViewById(R.id.single_mode);
        batchModeButton = (ImageButton) findViewById(R.id.batch_mode);
        doneButton = (ImageButton) findViewById(R.id.done);
        llDone = (LinearLayout) findViewById(R.id.layout_done);
        imagePreview = (ImageView) findViewById(R.id.preview);
        imagePreview.setVisibility(View.GONE);
        llDone.setVisibility(View.GONE);
        singleModeDot = (View) findViewById(R.id.single_mode_dot);
        batchModeDot = (View) findViewById(R.id.batch_mode_dot);
        singleModeLayout = (LinearLayout) findViewById(R.id.single_mode_layout);
        batchModeLayout = (LinearLayout) findViewById(R.id.batch_mode_layout);
        noOfImagesTextView = (TextView) findViewById(R.id.photo_number_indicator);
        noOfImagesTextView.setVisibility(View.GONE);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openUploadRecordsActivity();
            }
        });
        singleModeDot.setVisibility(View.INVISIBLE);
        batchModeDot.setVisibility(View.VISIBLE);
        singleModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singleModeDot.setVisibility(View.VISIBLE);
                batchModeDot.setVisibility(View.INVISIBLE);
                singleModeOn = true;
                Toast toast = Toast.makeText(CameraActivity.this, "Single Mode On",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });
        batchModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singleModeDot.setVisibility(View.INVISIBLE);
                batchModeDot.setVisibility(View.VISIBLE);
                singleModeOn = false;
                Toast toast = Toast.makeText(CameraActivity.this, "Batch Mode On", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });
        flashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!flashOn) {
                    flashOn = true;
                    flashButton.setImageResource(R.mipmap.flash_on_filled);
                    mPreview.turnOnFlash();
                } else {
                    flashOn = false;
                    flashButton.setImageResource(R.mipmap.flash_off_filled);
                    mPreview.turnOFfFlash();
                }
            }
        });
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get an image from the camera
                        mCamera.autoFocus(new Camera.AutoFocusCallback() {
                            public void onAutoFocus(boolean success, Camera camera) {
                                if (success) {
                                    if (mPreview.safeToTakePicture && mCamera != null) {
                                        MediaActionSound sound = new MediaActionSound();
                                        sound.play(MediaActionSound.SHUTTER_CLICK);
                                        mCamera.takePicture(null, null, mPicture);
                                        mPreview.safeToTakePicture = false;
                                    }
                                }
                            }
                        });
                    }
                }
        );
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mPreview.getHolder().removeCallback(mPreview);
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    public void openUploadRecordsActivity() {
        if (callForm.equals("ADDMORE")) {
            Intent intent = new Intent();
            intent.putStringArrayListExtra("listOfAddMoreImages", imagesAddressList);
            setResult(Activity.RESULT_OK, intent);
        } else {
            afterActivityResult = false;
            Intent intent = new Intent(this, UploadRecordsActivity.class);
            intent.putStringArrayListExtra("images_address_list", imagesAddressList);
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!afterActivityResult) {
            if (Utility.checkCameraHardware(this)) {
                releaseCamera();
                mCamera = getCameraInstance();
                Utility.setCameraDisplayOrientation(CameraActivity.this, 0, mCamera);
            } else {
                Toast.makeText(this, "No camera Found", Toast.LENGTH_SHORT).show();
            }
            mPreview = new CameraPreview(this, mCamera);
            preview.addView(mPreview);
            noOfImagesTaken = 0;
            imagePreview.setVisibility(View.GONE);
            noOfImagesTextView.setVisibility(View.GONE);
            llDone.setVisibility(View.GONE);
            batchModeLayout.setVisibility(View.VISIBLE);
            singleModeLayout.setVisibility(View.VISIBLE);
        } else {
            afterActivityResult = false;
        }
        if (flashOn) {
            mPreview.turnOnFlash();
        } else {
            mPreview.turnOFfFlash();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        afterActivityResult = true;
        if (requestCode == CAMERA_PREVIEW_ACTIVITY) {
            String action = data.getStringExtra("action");
            if (action.equals("retake")) {
                String address = imagesAddressList.get(imagesAddressList.size() - 1);
                imagesAddressList.remove(imagesAddressList.size() - 1);
                Utility.deleteImage(address);
                noOfImagesTaken--;
            }
            releaseCamera();
            mCamera = getCameraInstance();
            Utility.setCameraDisplayOrientation(CameraActivity.this, 0, mCamera);
            mCamera.startPreview();
            mPreview = new CameraPreview(this, mCamera);
            preview.addView(mPreview);
            if (singleModeOn && noOfImagesTaken == 1) {
                mCamera.stopPreview();
                openUploadRecordsActivity();
            } else {
                if (noOfImagesTaken == 0) {
                    imagePreview.setVisibility(View.GONE);
                    noOfImagesTextView.setVisibility(View.GONE);
                }
                if (singleModeOn) {
                    imagePreview.setVisibility(View.GONE);
                    noOfImagesTextView.setVisibility(View.GONE);
                    llDone.setVisibility(View.GONE);
                    batchModeLayout.setVisibility(View.VISIBLE);
                    singleModeLayout.setVisibility(View.VISIBLE);
                } else if (!singleModeOn && noOfImagesTaken > 0) {
                    imagePreview.setVisibility(View.VISIBLE);
                    noOfImagesTextView.setVisibility(View.INVISIBLE);
                    llDone.setVisibility(View.VISIBLE);
                    batchModeLayout.setVisibility(View.GONE);
                    singleModeLayout.setVisibility(View.GONE);
                    if (noOfImagesTaken > 1) {
                        noOfImagesTextView.setVisibility(View.VISIBLE);
                        noOfImagesTextView.setText(String.valueOf(noOfImagesTaken));
                    }
                    if (noOfImagesTaken > 0) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(
                                imagesAddressList.get(noOfImagesTaken - 1), options);
                        imagePreview.setImageBitmap(bitmap);
                        imagePreview.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (imagesAddressList.size() > 0) {
            for (int i = 0; i < imagesAddressList.size(); i++) {
                File file = new File(imagesAddressList.get(i));
                file.delete();
            }
        }
        super.onBackPressed();
    }
}