package com.example.cameraapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

public class UploadRecordsActivity extends AppCompatActivity {
    RecyclerView mGridView;
    private ImageAdapter imageAdapter;
    private ArrayList<String> imagesAddressList;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_records);
        mGridView = (RecyclerView) findViewById(R.id.recycler_view);
        imagesAddressList = getIntent().getStringArrayListExtra("images_address_list");
        mGridView.setLayoutManager(new GridLayoutManager(UploadRecordsActivity.this, 3));
        imageAdapter = new ImageAdapter(this, imagesAddressList);
        mGridView.setAdapter(imageAdapter);

    }

    @Override
    public void onBackPressed() {
        if (imagesAddressList.size() > 0) {
            for (int i = 0; i < imagesAddressList.size(); i++) {
                File file = new File(imagesAddressList.get(i));
                file.delete();
            }
        }
        finish();
        super.onBackPressed();
    }
}
