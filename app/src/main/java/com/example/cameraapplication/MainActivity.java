package com.example.cameraapplication;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    public static final String IMAGE_DIRECTORY_NAME = "Native";
    File mhcVerifiedImageFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvLaunchCamera = (TextView) findViewById(R.id.tv_CustomCamera);
        TextView tvNativeCamera = (TextView) findViewById(R.id.tv_NativeCamera);
        tvLaunchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                    try {
                        String cameraIdS = manager.getCameraIdList()[0];
                        CameraCharacteristics characteristics = manager
                                .getCameraCharacteristics(cameraIdS);
                        Integer deviceLevel = characteristics
                                .get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
                        if (deviceLevel != null)
                            if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY) {
                                Intent cameraOneActivity = new Intent(
                                        MainActivity.this, CameraActivity.class);
                                startActivity(cameraOneActivity);
                            } else {
                                Intent cameraTwoActivity = new Intent(
                                        MainActivity.this, Camera2Activity.class);
                                startActivity(cameraTwoActivity);
                            }
                    } catch (CameraAccessException e) {

                        e.printStackTrace();
                    }
                } else {
                    Intent cameraOneActivity = new Intent(
                            MainActivity.this, CameraActivity.class);
                    startActivity(cameraOneActivity);
                }
                // only to run camera 2 api
//                Intent cameraTwoActivity = new Intent(MainActivity.this, CameraOneNewActivity.class);
//                startActivity(cameraTwoActivity);
            }
        });

        tvNativeCamera.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View view) {
                // to get default camera of device
                String defaultCameraPackage = "";
                PackageManager packageManager = getPackageManager();
                List<ApplicationInfo> list = packageManager.getInstalledApplications(
                        PackageManager.GET_UNINSTALLED_PACKAGES);
                for (int n = 0; n < list.size(); n++) {
                    if ((list.get(n).flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                        Log.d("TAG", "Installed Applications : " + list.get(n).loadLabel(packageManager).toString());
                        Log.d("TAG", "package name  : " + list.get(n).packageName);
                        if (list.get(n).loadLabel(packageManager).toString().equalsIgnoreCase("Camera")) {
                            defaultCameraPackage = list.get(n).packageName;
                            break;
                        }
                    }
                }
                // create private folder image file path
                try {
                    mhcVerifiedImageFile = createImageFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // intent to launch native camera with custom path
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri uri = FileProvider.getUriForFile(MainActivity.this,
                        BuildConfig.APPLICATION_ID, mhcVerifiedImageFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                intent.putExtra("android.intent.extra.quickCapture",true);
                intent.setPackage(defaultCameraPackage);
                startActivityForResult(intent, 1);
            }
        });
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = this.getExternalFilesDir(IMAGE_DIRECTORY_NAME);
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // result of camera, gallery and google place api
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                 //   deleteLastCapturedImage();
                } else if (resultCode == RESULT_CANCELED) {
                    //delete custom path file if image capture cancel
                    mhcVerifiedImageFile.delete();
                }
                break;
        }
    }

    public void deleteLastCapturedImage() {

        // delete latest image from gallery(DCIM folder)
        String[] projection = {
                MediaStore.Images.ImageColumns.SIZE,
                MediaStore.Images.ImageColumns.DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATA,
                BaseColumns._ID
        };
        Cursor cursor = null;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        try {
            if (uri != null) {
                cursor = getContentResolver().query(uri, projection, null, null, null);
            }
            if ((cursor != null) && (cursor.moveToLast())) {
                ContentResolver contentResolver = getContentResolver();
                int i = contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        BaseColumns._ID + "=" + cursor.getString(
                                cursor.getColumnIndex(BaseColumns._ID)), null);
                Log.v("delete image", "Number of column deleted : " + i);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
