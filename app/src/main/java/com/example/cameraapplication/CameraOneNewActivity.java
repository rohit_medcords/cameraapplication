package com.example.cameraapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by user on 25/1/16.
 */
public class CameraOneNewActivity extends Activity implements SurfaceHolder.Callback {
    private SurfaceView mSurfaceView = null;
    private LinearLayout lButtonLayout;
    private SurfaceHolder mSurfaceHolder = null;
    private static Camera mCamera = null;
    private boolean mIsCapturing = false;
    private int mPreviewHeight, mPreviewWidth, flag = 0, mCameraContainerWidth = 0;
    private CheckBox cbFrontCamera, cbFlash;
    private ImageView ivRecord;
    private MyFileObserver fb;
    private Camera.PictureCallback rawCallback, jpegCallback;
    private Camera.ShutterCallback shutterCallback;
    private ProgressDialog loading;
    private String path, path1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_one_camera);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                ex.printStackTrace();
                releaseCamera();
            }
        });
        OrientationEventListener myOrientationEventListener = new OrientationEventListener(
                this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int arg0) {
                path = String.valueOf(arg0);
            }
        };
        if (myOrientationEventListener.canDetectOrientation()) {
            myOrientationEventListener.enable();
        }

        lButtonLayout = (LinearLayout) findViewById(R.id.ll_ButtonLayout);
        cbFlash = (CheckBox) findViewById(R.id.cb_Flash);

        findViewById(R.id.tv_Save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("path", path1);
                setResult(RESULT_OK, intent);
                CameraOneNewActivity.this.finish();
            }
        });

        findViewById(R.id.tv_Cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File imageFile = new File(path1);
                if (imageFile.exists()) {
                    imageFile.delete();
                }
                Intent intent = new Intent();
                intent.putExtra("Error", "Error Occured, Image Not Captured");
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        findViewById(R.id.tv_Retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File imageFile = new File(path1);
                if (imageFile.exists()) {
                    imageFile.delete();
                }
                lButtonLayout.setVisibility(View.GONE);
                if (mIsCapturing) {
                    stopCapturing();
                } else {
                    ivRecord.setVisibility(View.VISIBLE);
                    cbFrontCamera.setVisibility(View.VISIBLE);
                    captureImage();
                }
            }
        });

        cbFrontCamera = (CheckBox) findViewById(R.id.cb_FrontCamera);
        cbFrontCamera.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mIsCapturing)
                    stopCapturing();
                releaseCamera();
                mCamera = getCamera();
                setAutoFocus();
                if (cbFrontCamera.isChecked() && !hasFlash()) {
                    cbFlash.setVisibility(View.INVISIBLE);
                } else {
                    cbFlash.setVisibility(View.VISIBLE);
                }
                if (cbFlash.getVisibility() == View.VISIBLE) {
                    if (cbFrontCamera.isChecked() && cbFlash.isChecked()) {
                        cbFlash.setChecked(false);
                    } else if ((!cbFrontCamera.isChecked()) && cbFlash.isChecked()) {
                        cbFlash.setChecked(false);
                    }
                }
            }
        });
        captureImage();
    }

    private void captureImage() {
        mCamera = getCamera();
        if (!hasFlash()) {
            cbFlash.setVisibility(View.INVISIBLE);
        } else {
            cbFlash.setVisibility(View.VISIBLE);
        }
        //camera preview
        mSurfaceView = (SurfaceView) findViewById(R.id.sv_Preview);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mCameraContainerWidth = mSurfaceView.getLayoutParams().width;
        ivRecord = (ImageView) findViewById(R.id.iv_Record);
        ivRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsCapturing) {
                    stopCapturing();
                } else {
                    if (mCamera != null) {
                        cbFlash.setVisibility(View.INVISIBLE);
                        cbFrontCamera.setVisibility(View.INVISIBLE);
                        mIsCapturing = true;
                        mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
                        stopCapturing();
                    } else {
                    }
                }
            }
        });
        setAutoFocus();
        rawCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
            }
        };

        shutterCallback = new Camera.ShutterCallback() {
            public void onShutter() {
            }
        };

        jpegCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                FileOutputStream outStream = null;
                try {
                    File mediaFile_path = new File(
                            Environment.getExternalStorageDirectory(), "IMG-" + System.currentTimeMillis() + ".jpg");
                    String imagePath = mediaFile_path.getPath();
                    fb = new MyFileObserver(mediaFile_path.getAbsolutePath(), FileObserver.CLOSE_WRITE);
                    fb.startWatching();
                    outStream = new FileOutputStream(mediaFile_path);
                    outStream.write(data);
                    outStream.close();
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                    try {
                        Matrix matrix = new Matrix();

                        if (flag == 1) {
                            if (Integer.parseInt(path) > 250 && Integer.parseInt(path) < 300) {
                                matrix.postRotate(0);
                            } else if (Integer.parseInt(path) >= 0 && Integer.parseInt(path) < 50) {
                                matrix.postRotate(270);
                            } else if (Integer.parseInt(path) > 300) {
                                matrix.postRotate(270);
                            } else if (Integer.parseInt(path) > 50 && Integer.parseInt(path) < 100) {
                                matrix.postRotate(180);
                            }

                        } else {
                            if (Integer.parseInt(path) > 250 && Integer.parseInt(path) < 300) {
                                matrix.postRotate(0);
                            } else if (Integer.parseInt(path) >= 0 && Integer.parseInt(path) < 50) {
                                matrix.postRotate(90);
                            } else if (Integer.parseInt(path) > 300) {
                                matrix.postRotate(90);
                            } else if (Integer.parseInt(path) > 50 && Integer.parseInt(path) < 100) {
                                matrix.postRotate(180);
                            }
                        }

                        while (mediaFile_path.length() > 4000000) {
                            bitmap = BitmapFactory.decodeFile(imagePath);
                            outStream = new FileOutputStream(imagePath);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, outStream);
                            outStream.flush();
                            outStream.close();
                            mediaFile_path = new File(imagePath);
                        }
                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                        outStream = new FileOutputStream(mediaFile_path);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, outStream);
                        outStream.flush();
                        outStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mCamera.startPreview();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
            }
        };
    }

    private void stopCapturing() {
        mCamera.lock();
        ivRecord.setVisibility(View.INVISIBLE);
        if (cbFlash.isChecked()) {
            cbFlash.setChecked(false);
        }
        loading = ProgressDialog.show(this, null, "Processing please wait...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                cbFlash.setVisibility(View.INVISIBLE);
                cbFrontCamera.setVisibility(View.INVISIBLE);
                lButtonLayout.setVisibility(View.VISIBLE);
                mIsCapturing = false;
                loading.dismiss();
            }
        }, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseCamera();
    }

    private Camera getCamera() {
        flag = 0;
        releaseCamera();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cbFrontCamera.isChecked()) {
                try {
                    lButtonLayout.setVisibility(View.GONE);
                    mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    mCamera.setDisplayOrientation(90);
                    mCamera.startPreview();
                    mCamera.setPreviewDisplay(mSurfaceHolder);
                    flag = 1;
                    return mCamera;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    lButtonLayout.setVisibility(View.GONE);
                    mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    mCamera.setDisplayOrientation(90);
                    mCamera.startPreview();
                    mCamera.setPreviewDisplay(mSurfaceHolder);
                    return mCamera;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mIsCapturing) {
            stopCapturing();
            File imageFile = new File(path1);
            if (imageFile.exists()) {
                imageFile.delete();
            }
        }
        releaseCamera();
        finish();
    }

    private Camera.Size getBestPreviewSize(Camera.Parameters parameters) {
        Camera.Size result = null;
        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width < size.height) continue; //we are only interested in landscape variants
            if (result == null) {
                result = size;
            } else {
                int resultArea = result.width * result.height;
                int newArea = size.width * size.height;
                if (newArea > resultArea) {
                    result = size;
                }
            }
        }
        return (result);
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        cbFlash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cbFlash.isChecked()) {
                    Camera.Parameters parameters = mCamera.getParameters();
                    parameters.setRecordingHint(true);
                    String flash = parameters.getFlashMode();
                    if (!flash.equals(Camera.Parameters.FLASH_MODE_TORCH)) {
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    }
                    Camera.Size size = getBestPreviewSize(parameters);
                    mCamera.setDisplayOrientation(90);
                    mCamera.setParameters(parameters);
                    int newHeight = size.height / (size.width / mCameraContainerWidth);
                    mSurfaceView.getLayoutParams().height = newHeight;
                } else {
                    Camera.Parameters parameters = mCamera.getParameters();
                    parameters.setRecordingHint(true);
                    try {
                        String flash = parameters.getFlashMode();
                        if (flash.equals(Camera.Parameters.FLASH_MODE_TORCH)) {
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Camera.Size size = getBestPreviewSize(parameters);
                    mCamera.setDisplayOrientation(90);
                    mCamera.setParameters(parameters);
                    int newHeight = size.height / (size.width / mCameraContainerWidth);
                    mSurfaceView.getLayoutParams().height = newHeight;
                }
            }
        });
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mPreviewHeight = mCamera.getParameters().getPreviewSize().height;
        mPreviewWidth = mCamera.getParameters().getPreviewSize().width;
        mCamera.stopPreview();
        try {
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mIsCapturing) {
            stopCapturing();
        }
        releaseCamera();
    }

    private boolean hasFlash() {
        List<String> supportedFlashModes = mCamera.getParameters().getSupportedFlashModes();
        if (supportedFlashModes == null) {
            return false;
        }
        for (String flashMode : supportedFlashModes) {
            if (Camera.Parameters.FLASH_MODE_ON.equals(flashMode)) {
                return true;
            }
        }
        return false;
    }

    private void setAutoFocus() {
        List<String> supportedFocusModes = mCamera.getParameters().getSupportedFocusModes();
        boolean hasAutoFocus = supportedFocusModes != null &&
                supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        if (hasAutoFocus) {
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            mCamera.setParameters(parameters);
        }
        mCamera.startPreview();
    }

    private class MyFileObserver extends FileObserver {
        public MyFileObserver(String path, int mask) {
            super(path, mask);
            path1 = path;
        }

        public void onEvent(int event, String path) {
            fb.stopWatching();
            stopCapturing();
        }
    }

    @Override
    public void onBackPressed() {
        if (mIsCapturing) {
            stopCapturing();
            File imageFile = new File(path1);
            if (imageFile.exists()) {
                imageFile.delete();
            }
        }
        super.onBackPressed();
        finish();
    }
}