package com.example.cameraapplication;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.ContentValues.TAG;

@SuppressWarnings("deprecation")
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    CameraActivity activity;
    Camera.Parameters params;

    public boolean safeToTakePicture = false;
    Context context;
    List<Camera.Size> sizeList;
    List<Camera.Size> mSupportedPreviewSizes;
    Camera.Size mPreviewSize;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        stopPreviewAndFreeCamera();
        mCamera = camera;
        this.context = context;
        activity = (CameraActivity) context;
        params = mCamera.getParameters();
        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

        List<String> focusModes = params.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        List<String> flashModes = params.getSupportedFlashModes();
        if (flashModes != null && flashModes.contains(android.hardware.Camera.Parameters.FLASH_MODE_AUTO)) {
            params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        }

        sizeList = camera.getParameters().getSupportedPreviewSizes();
        Collections.sort(sizeList, new Comparator<Camera.Size>() {

            public int compare(final Camera.Size a, final Camera.Size b) {
                return a.width * a.height - b.width * b.height;
            }
        });

//        Camera.Size bestSize = null;
//        bestSize = sizeList.get(sizeList.size() - 1);
//        for (int i = 1; i < sizeList.size(); i++) {
//            if ((sizeList.get(i).width * sizeList.get(i).height) >
//                    (bestSize.width * bestSize.height)) {
//                bestSize = sizeList.get(i);
//            }
//        }
        //params.setPreviewSize(bestSize.width, bestSize.height);

        //        for (int i = 1; i < allSizes.size(); i++) {
//            if ((allSizes.get(i).width * allSizes.get(i).height) >
//                    (bestSizePic.width * bestSizePic.height)) {
//                bestSizePic = allSizes.get(i);
//            }
//        }

//        Toast.makeText(context, "Preview :- " + bestSize.width + " " + bestSize.height + " Picture :- "
//                + bestSizePic.width + " " + bestSizePic.height, Toast.LENGTH_LONG).show();

        List<Camera.Size> allSizes = camera.getParameters().getSupportedPictureSizes();
        Collections.sort(allSizes, new Comparator<Camera.Size>() {

            public int compare(final Camera.Size a, final Camera.Size b) {
                return a.width * a.height - b.width * b.height;
            }
        });
        Camera.Size bestSizePic = null;
        bestSizePic = allSizes.get(allSizes.size() - 1);

        params.setPictureSize(bestSizePic.width, bestSizePic.height);

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break; //Natural orientation
            case Surface.ROTATION_90:
                degrees = 90;
                break; //Landscape left
            case Surface.ROTATION_180:
                degrees = 180;
                break;//Upside down
            case Surface.ROTATION_270:
                degrees = 270;
                break;//Landscape right
        }
        int rotate = (info.orientation - degrees + 360) % 360;
        params.setRotation(rotate);
        mCamera.setParameters(params);

        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null)
            mCamera.stopPreview();
        stopPreviewAndFreeCamera();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

        if (mHolder.getSurface() == null)
            return;

        Camera.Size size = getOptimalPreviewSize(sizeList, w, h);
        params = mCamera.getParameters();
        params.setPreviewSize(size.width, size.height);
        mCamera.setParameters(params);

        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
            safeToTakePicture = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopPreviewAndFreeCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    public void turnOnFlash() {
        params = mCamera.getParameters();
        params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
        mCamera.setParameters(params);
    }

    public void turnOFfFlash() {
        params = mCamera.getParameters();
        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        mCamera.setParameters(params);
    }
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        // We purposely disregard child measurements because act as a
//        // wrapper to a SurfaceView that centers the camera preview instead
//        // of stretching it.
//        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
//        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
//        setMeasuredDimension(width, height);
//
//        if (mSupportedPreviewSizes != null) {
//            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
//        }
//    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
        }

        if (mPreviewSize != null) {
            float ratio;
            if (mPreviewSize.height >= mPreviewSize.width)
                ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
            else
                ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;
            float camHeight = (int) (width * ratio);
            float newCamHeight;
            float newHeightRatio;

            if (camHeight < height) {
                newHeightRatio = (float) height / (float) mPreviewSize.height;
                newCamHeight = (newHeightRatio * camHeight);
                Log.e(TAG, camHeight + " " + height + " " + mPreviewSize.height + " "
                        + newHeightRatio + " " + newCamHeight);
                //setMeasuredDimension((int) (width * newHeightRatio), (int) newCamHeight);
                Log.e(TAG, mPreviewSize.width + " | " + mPreviewSize.height + " | ratio - "
                        + ratio + " | H_ratio - " + newHeightRatio + " | A_width - "
                        + (width * newHeightRatio) + " | A_height - " + newCamHeight);
            } else {
                newCamHeight = camHeight;
                //setMeasuredDimension(width, (int) newCamHeight);
                Log.e(TAG, mPreviewSize.width + " | " + mPreviewSize.height + " | ratio - "
                        + ratio + " | A_width - " + (width) + " | A_height - " + newCamHeight);
            }
            // One of these methods should be used, second method squishes preview slightly
            setMeasuredDimension(width, (int) (width * ratio));
            //        setMeasuredDimension((int) (width * ratio), height);
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.05;
        double targetRatio = (double) w / h;

        if (sizes == null) return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        // Find size
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    private Camera.Size getBestPreviewSize(int w, int h, List<Camera.Size> sizes) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.height / size.width;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    private Camera.Size getBestPreviewSize_1(int width, int height,
                                             List<Camera.Size> psizes) {
        Log.d("Surface sizes list", " width " + width + " height " + height);
        for (int i = 1; i < psizes.size(); i++) {
            Camera.Size cs = psizes.get(i);
            Log.d("preview sizes list", " width " + cs.width + " height " + cs.height
            );
        }
        Camera.Size result = null;

        float surface = (float) ((width * 1.0) / height);
        result = psizes.get(0);
        for (int i = 1; i < psizes.size(); i++) {
            Camera.Size cs = psizes.get(i);
            float cv = ((float) ((cs.width * 1.0) / cs.height) - surface);
            float rv = ((float) ((result.width * 1.0) / result.height) - surface);
            float dv = cv - rv;
            if (Math.abs(cv) < Math.abs(rv)) {
                result = cs;
            }
        }
        return result;
    }
}