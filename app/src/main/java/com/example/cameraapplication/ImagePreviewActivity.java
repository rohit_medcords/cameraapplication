package com.example.cameraapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class ImagePreviewActivity extends AppCompatActivity {
    ImageView retakeButton, doneButton;
    ImageView imagePreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        imagePreview = (ImageView) findViewById(R.id.camera_preview);
        retakeButton = (ImageView) findViewById(R.id.retake_button);
        doneButton = (ImageView) findViewById(R.id.done);

        Bundle bundle = getIntent().getExtras();
        String filePath = (String) bundle.get("FilePath");
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);
        imagePreview.setImageBitmap(bitmap);

        retakeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("action", "retake");
                setResult(CameraActivity.CAMERA_PREVIEW_ACTIVITY, intent);
                finish();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("action", "done");
                setResult(CameraActivity.CAMERA_PREVIEW_ACTIVITY, intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("action", "retake");
        setResult(CameraActivity.CAMERA_PREVIEW_ACTIVITY, intent);
        super.onBackPressed();
    }
}